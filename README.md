# Oauth 2 Server example

## Getting started

The public/private key pair is used to sign and verify JWTs transmitted. 
The Authorization Server possesses the private key to sign tokens and the Resource Server possesses the corresponding public key to verify the signatures.

```bash
openssl genrsa -out private.key 2048
openssl rsa -in private.key -pubout -out public.key
```

That's it! Now go build something cool.
